vim.wo.spell = true
vim.wo.colorcolumn = "80"
-- vim.bo.cursorcolumn = true
vim.bo.textwidth = 80
vim.bo.formatexpr = "80"
